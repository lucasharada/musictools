package core;
import java.io.File;

public class FileInsertArtistName{
	public static void main(String[] args) {
		// change file names in 'Directory':
		String absolutePathUnit = "E:\\Users\\Lucas\\Desktop\\Sam Smith - In The Lonely Hour (Drowning Shadows Edition) [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]";
		String artistName = "Sam Smith -";
		File unit = new File(absolutePathUnit);
		if (unit.isDirectory()){
			String absolutePath = unit.getAbsolutePath();
			File[] filesInDir = unit.listFiles();
			for(File file:filesInDir) {
				String name = file.getName();
				//String newName = artistName + " - " + name;
				String newName = artistName + " " + name;
				String newPath = absolutePath + "\\" + newName;
				file.renameTo(new File(newPath));
				System.out.println(name + " changed to " + newName);
			}
		}
	} // close main()
} // close class