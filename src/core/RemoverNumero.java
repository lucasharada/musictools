package core;
import java.io.*;

public class RemoverNumero {
	public static void main(String[] args) throws IOException {
		// change file names in 'Directory':
		String absolutePath = "E:\\Users\\Lucas\\Desktop\\Sam Smith - In The Lonely Hour (Drowning Shadows Edition) [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]";
		File dir = new File(absolutePath);

		File[] filesInDir = dir.listFiles();
		for(File file:filesInDir) {
			String name = file.getName();
			String newName = name.substring(name.indexOf(' ')+1);
			String newPath = absolutePath + "\\" + newName;
			file.renameTo(new File(newPath));
			System.out.println(name + " changed to " + newName);
		}
	} // close main()
} // close class