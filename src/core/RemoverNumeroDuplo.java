package core;
import java.io.*;

public class RemoverNumeroDuplo {
	public static void main(String[] args) throws IOException {
		// change file names in 'Directory':
		String absolutePathUnit = "E:\\Users\\Lucas\\Desktop\\CHARMANDER";
		File unit = new File(absolutePathUnit);
		File[] dirsInUnit = unit.listFiles();
		for (File dir:dirsInUnit){
			String absolutePath = dir.getAbsolutePath();
			//File dir = new File(absolutePath);
			if (dir.isDirectory()){
				File[] filesInDir = dir.listFiles();
				for(File file:filesInDir) {
					String name = file.getName();
					String newName = name.substring(0, 3) + name.substring(6);
					String newPath = absolutePath + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
				}
			}
		}
	} // close main()
} // close class