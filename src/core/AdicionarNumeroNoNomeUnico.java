package core;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AdicionarNumeroNoNomeUnico {
	public static void main(String[] args) throws IOException {
		// change file names in 'Directory':
		String absolutePath= "G:\\The Swon Brothers Self Titled + Timeless";
		File dir = new File(absolutePath);
		if (dir.isDirectory()){
			File[] filesInDir = dir.listFiles();
			int i = 0;
			for(File file:filesInDir) {
				String name = file.getName();
				Path path = Paths.get(absolutePath + "\\" + name);
				byte[] data = Files.readAllBytes(path);
				i = data[data.length-2];
				if (i < 10){
					String newName = "0" + i + "." + name;
					String newPath = absolutePath + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
				}
				else {
					String newName = i + "." + name;
					String newPath = absolutePath + "\\" + newName;
					file.renameTo(new File(newPath));
					System.out.println(name + " changed to " + newName);
				}
			}
		} // close main()
	} // close class
}
